/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vtcup.molaya.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.vtcup.molaya.app.ControllerActiveDirectory;
import org.vtcup.molaya.db.DataBaseConnection;

/**
 *
 * @author molaya
 */
@WebServlet(name = "SearchObject", urlPatterns = {"/SearchObject"})
public class SearchObject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        DataBaseConnection db = new DataBaseConnection();
        ControllerActiveDirectory ad = new ControllerActiveDirectory();
        HttpSession session = request.getSession();
        ResultSet rs = null;

        String user = request.getParameter("username");
        String type = request.getParameter("type");
        String objectAd = null;
        String userid = null;
        String userinfo = null;
        String msg = null;

        db.connect();
        boolean isReal = false;

        //Validating session.
        userid = (String) session.getAttribute("userid");
        System.out.println("userid: " + userid);
        userinfo = (String) session.getAttribute("userinfo");
        System.out.println("userinfo: " + userinfo);

        if (type == null) {
            type = "user";
        }

        if (userid == null || userinfo == null) {
            session.invalidate();
            response.sendRedirect("index.html?error=440");
        }
        //Getting user application information...
        /*try {
         rs = db.read("SELECT userid from 'TBLSESSIONS' where sessionid=\"" + session.getId() + "\"");
         userid = rs.getString("userid");
         rs.close();
         rs = db.read("SELECT * from 'TBLUSERS' where userid=\"" + userid + "\"");
         userinfo = rs.getString("userinfo");
         rs.close();
         } catch (SQLException ex) {
         Logger.getLogger(resetPass.class.getName()).log(Level.SEVERE, null, ex);
         db.disconnect();
         response.sendRedirect("index.html?error=" + ex);
         }
         */

        System.out.println("SEARCHING TOOL OBJECT");
        //return true if object was found on AD.
        isReal = ad.getObjectFromAD(user, type);
        objectAd = ad.getObjectString();
        System.out.println("STATUS SEARCH: " + isReal + " OF " + type + " NAME:" + user);
        out.println("<!DOCTYPE html>\n"
                + "<!--\n"
                + "We are working for improving your Active Directory experience.\n"
                + "You must validate all info with the application manager.\n"
                + "If you ahve any sugestion, please send email to olaya.marlon@gmail.com.\n"
                + "-->\n"
                + "<html lang=\"en\">\n"
                + "    <head>\n"
                + "        <meta charset=\"utf-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "        <title>Vertical Active Directory Tools</title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "        <link href=\"css/animate.css\" rel=\"stylesheet\">\n"
                + "    </head>\n"
                + "    <body>\n"
                + "    <h2 style=\"text-align: center;\">Search Result</h2>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"modal-dialog\">\n"
                + "                <div class=\"modal-content\">\n"
                + "                    <div class=\"modal-header\">\n"
                + "                        <!--<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>-->\n"
                + "                        <div class=\"row text-center animated fadeInDown\"><img src=\"images/logocrediorbe.png\"></div>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-body \">\n"
                + "                        <dl class=\"dl-horizontal\">\n"
                + "                        <h3>\n"
                + "                                <dt>Username:</dt> <dd>" + user + "</dd>\n"
                + "                                <dt>Type:</dt> <dd>" + type + "</dd>\n");
        if (isReal) {
            System.out.println("SEARCH OK" + objectAd);
            out.println("                          <dt>Name:</dt> <dd>" + objectAd.split("=")[1].split(",")[0] + "</dd>\n"
                    + "                            <dt>Office:</dt> <dd>" + objectAd.split("=")[3].split(",")[0] + "</dd>\n"
                    + "                            <dt>Group:</dt> <dd>" + ad.getGroupOfObjectDSN(objectAd, null, true) + "</dd>\n"
                    + "                            <dt>Status:</dt> <dd><span class=\"label label-success animated tada\">Success " + type + " search</span></dd>\n");

            db.write("INSERT INTO TBLLOGS (userid, logname, loginfo) VALUES (" + userid + ", 'SEARCHOBJECT', 'SEARCH " + type + " WITH NAME: " + user + " STATUS: SUCCESS' )");

        } else {
            if (objectAd != null) {
                msg = "We can't find the " + type + " " + objectAd;
            } else {
                msg = "Wrong " + type + ": " + ad.getErrors();
            }
            out.println("                                <dt>Status:</dt> <dd><span class=\"label label-danger\">Unsuccessfull</span></dd>\n"
                    + "                        </h3> \n"
                    + "                                <dt>Reason:</dt> <dd>There isn't " + type + " with name " + user.toUpperCase() + "<br>" + msg + "</dd>");
            System.out.println("SEARCH WRONG!!");
            db.write("INSERT INTO TBLLOGS (userid, logname, loginfo) VALUES (" + userid + ", 'SEARCHOBJECT', 'SEARCH " + type + " WITH NAME: " + user + " STATUS: UNSUCCESSFULL' )");
        }
        out.println("                            </dl>  \n"
                + "                    </div>\n"
                + "                    <div class=\"modal-footer\">\n"
                + "                        <button class=\"btn btn-lg btn-default\" onclick=\"myFunction()\" ><span class=\"glyphicon glyphicon-print\"></span> Print</button>\n"
                + "                        <button class=\"btn btn-lg btn-success\" data-toggle=\"modal\" data-target=\"#chgModal\" ><span class=\"glyphicon glyphicon-globe\"></span></button>\n"
                + "                        <button class=\"btn btn-lg btn-primary animated swing\" onclick=\"window.location='home.html'\"><span class=\"glyphicon glyphicon-home\"></span> Go back!</button>"
                + "                    </div>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <footer class=\"footer\">\n"
                + "            <div class=\"container\">\n"
                + "                <div class=\"row text-center animated fadeInUp\"><img src=\"images/logo.png\"></div>\n"
                + "            </div>\n"
                + "        </footer>\n"
                + "        <div class=\"modal fade\" id=\"offModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"smallModal\" aria-hidden=\"true\">\n"
                + "        <div class=\"modal-dialog modal-sm\">\n"
                + "            <div class=\"modal-content\">\n"
                + "                 <div class=\"modal-header\">\n"
                + "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n"
                + "                    <h3 class=\"modal-title\" id=\"myModalLabel\"><div class=\"row text-center\"><img src=\"images/logo.ico\"> Delete!</div></h3>\n"
                + "                 </div>\n"
                + "                 <form method=\"post\" action=\"DeleteObject\">\n"
                + "                     <div class=\"modal-body \">\n"
                + "                         <h4>Are you sure you want to delete this user?</h4>\n"
                + "                     </div>\n"
                + "                     <div class=\"modal-footer\">\n"
                + "                         <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n"
                + "                         <input type=\"text\" style=\"text-align: center\" hidden=\"yes\" name=\"username\" value=\"" + user + "\">\n"
                + "                         <input type=\"text\" style=\"text-align: center\" hidden=\"yes\" name=\"type\" value=\"" + type + "\">\n"
                + "                         <input type=\"submit\" class=\"btn btn-danger animated bounce\" value=\"I am Sure!\">\n"
                + "                     </div>\n"
                + "                 </form>\n"
                + "                 </div>\n"
                + "            </div>\n"
                + "        </div>"
                + "    <div class=\"modal fade\" id=\"chgModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"smallModal\" aria-hidden=\"true\">\n"
                + "        <div class=\"modal-dialog modal-body\">\n"
                + "            <div class=\"modal-content\">\n"
                + "                <div class=\"modal-header\">\n"
                + "                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n"
                + "                    <div class=\"row text-center\"><img src=\"images/logocrediorbe.png\"><h4 class=\"modal-title\" id=\"myModalLabel\">Create New...</h4></div>\n"
                + "                </div>\n"
                + "                <form method=\"post\" action=\"EditNavigationProfile\" >\n"
                + "                    <div class=\"modal-body\">\n"
                + "                        <h3 class=\"text-center\">Please fill out the form below:</h3>\n"
                + "                        <div class=\"form-group text-center\">                                \n"
                + "                            <input type=\"text\" style=\"text-transform: lowercase; text-align: center\" class=\"input-lg animated bounceIn\" hidden=\"yes\" name=\"samid\" value=\"" + user + "\" required>\n"
                + "                            <input type=\"text\" style=\"text-transform: lowercase; text-align: center\" class=\"input-lg animated bounceIn\" hidden=\"yes\" name=\"type\" value=\"" + type + "\">\n"
                + "                            <h4 class=\"modal-tittle\" id=\"myModalLabel\">New Group*:</h4>\n"
                + "                            <select class=\"form-control\" name=\"profile\" style=\"text-transform: uppercase; text-align: center\">\n"
                + "                                <option value=\"CrediALL\" selected=\"\">CrediALL</option>\n"
                + "                                <option value=\"CrediOtherPages\">CrediOTHER</option>\n"
                + "                                <option value=\"CrediVIP\">CrediVIP</option>\n"
                + "                            </select>\n"
                + "                        </div>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-footer\">\n"
                + "                        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n"
                + "                        <input type=\"submit\" class=\"btn btn-primary animated swing\" value=\"Change\">\n"
                + "                    </div>\n"
                + "                </form>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "    </div>\n"
                + ""
                + "        <script>\n"
                + "            function myFunction() {\n"
                + "                window.print();\n"
                + "            }\n"
                + "            function goTo() {\n"
                + "                window.print();\n"
                + "            }\n"
                + "            function callServlet() {"
                + "                document.forms[0].submit;"
                + "            }"
                + "        </script>"
                + "        <!--<script src=\"https://code.jquery.com/jquery.js\"></script>-->\n"
                + "        <script src=\"js/jquery-1.11.2.js\"></script>\n"
                + "        <script src=\"js/bootstrap.min.js\"></script>\n"
                + "    </body>\n"
                + "\n"
                + "</html>");
        db.disconnect();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
