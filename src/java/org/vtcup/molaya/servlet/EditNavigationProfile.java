/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vtcup.molaya.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.vtcup.molaya.app.ControllerActiveDirectory;
import org.vtcup.molaya.db.DataBaseConnection;

/**
 *
 * @author molaya
 */
@WebServlet(name = "EditNavigationProfile", urlPatterns = {"/EditNavigationProfile"})
public class EditNavigationProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        DataBaseConnection db = new DataBaseConnection();
        ControllerActiveDirectory ad = new ControllerActiveDirectory();
        HttpSession session = request.getSession();
        ResultSet rs = null;

        String samid = request.getParameter("samid");
        String type = request.getParameter("type");
        String group = request.getParameter("profile");
        String objectAd = null;
        String userid = null;
        String userinfo = null;
        String msg = null;

        db.connect();
        boolean isEdited = false;

        //Validating session.
        userid = (String) session.getAttribute("userid");
        System.out.println("userid: " + userid);
        userinfo = (String) session.getAttribute("userinfo");
        System.out.println("userinfo: " + userinfo);

        if (userid == null || userinfo == null) {
            session.invalidate();
            response.sendRedirect("index.html?error=440");
            return;
        }
        boolean isReal = false;
        System.out.println("TOOL TO CHANGE NAVIGATION PROFILE");
        //return true if object was found on AD.
        isReal = ad.getObjectFromAD(samid, "user" );
        objectAd = ad.getObjectString();
        isEdited = ad.changeObjectGroup(group, samid, "user", false);
        System.out.println("STATUS CHANGED: " + isEdited + " OF " + type + " NAME:" + samid);
        out.println("<!DOCTYPE html>\n"
                + "<!--\n"
                + "We are working for improving your Active Directory experience.\n"
                + "You must validate all info with the application manager.\n"
                + "If you ahve any sugestion, please send email to olaya.marlon@gmail.com.\n"
                + "-->\n"
                + "<html lang=\"en\">\n"
                + "    <head>\n"
                + "        <meta charset=\"utf-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "        <title>Vertical Active Directory Tools</title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "        <link href=\"css/animate.css\" rel=\"stylesheet\">\n"
                + "    </head>\n"
                + "    <body>\n"
                + "    <h2 style=\"text-align: center;\">Registry add action</h2>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"modal-dialog\">\n"
                + "                <div class=\"modal-content\">\n"
                + "                    <div class=\"modal-header\">\n"
                + "                        <!--<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>-->\n"
                + "                        <div class=\"row text-center animated fadeInDown\"><img src=\"images/logocrediorbe.png\"></div>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-body \">\n"
                + "                        <dl class=\"dl-horizontal\">\n"
                + "                        <h3>\n"
                + "                                <dt>Alias:</dt> <dd>" + samid + "</dd>\n"
                + "                                <dt>Type:</dt> <dd>" + type + "</dd>\n");
        if (isReal) {
            System.out.println("SEARCH OK" + objectAd);
            out.println("                          <dt>Name:</dt> <dd>" + objectAd.split("=")[1].split(",")[0] + "</dd>\n"
                    + "                            <dt>Office:</dt> <dd>" + objectAd.split("=")[3].split(",")[0] + "</dd>\n");
        }
        if (isEdited) {
//            isEdited = ad.addGroupToUser(group, samid);
            System.out.println("Group USER OK" + samid);
            out.println("                            <dt>New Group:</dt> <dd>" + ad.getGroupOfObjectDSN(samid, null, false) + "</dd>\n");

            out.println("                            <dt>Status:</dt> <dd><span class=\"label label-success animated tada\">Success: Navigation profile of was changed</span></dd>\n");

            db.write("INSERT INTO TBLLOGS (userid, logname, loginfo) VALUES (" + userid + ", 'CHANGEPROFILE', 'CHANGE PROFILE WITH NAME: " + samid + " to "+ group +" STATUS: SUCCESS' )");

        } else {
            msg = ad.getErrors();
            out.println("                                <dt>Status:</dt> <dd><span class=\"label label-danger\">Unsuccessfull</span></dd>\n"
                    + "                        </h3> \n"
                    + "                                <dt>Reason:</dt> <dd>There was a problem changing profile of " + samid.toUpperCase() + "<br>" + msg + "</dd>");
            System.out.println("ADD WRONG!!");
            db.write("INSERT INTO TBLLOGS (userid, logname, loginfo) VALUES (" + userid + ", 'CHANGEPROFILE', 'ADD USER  WITH NAME: " + samid + " STATUS: UNSUCCESSFULL' )");
        }
        out.println("                            </dl>  \n"
                + "                    </div>\n"
                + "                    <div class=\"modal-footer\">\n"
                + "                        <button class=\"btn btn-lg btn-default\" onclick=\"myFunction()\" ><span class=\"glyphicon glyphicon-print\"></span> Print</button>\n"
                + "                        <button class=\"btn btn-lg btn-primary animated swing\" onclick=\"window.location='home.html'\"><span class=\"glyphicon glyphicon-home\"></span> Go back!</button>"
                + "                    </div>\n"
                + "\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <footer class=\"footer\">\n"
                + "            <div class=\"container\">\n"
                + "                <div class=\"row text-center animated fadeInUp\"><img src=\"images/logo.png\"></div>\n"
                + "            </div>\n"
                + "        </footer>\n"
                + "        <script>\n"
                + "            function myFunction() {\n"
                + "                window.print();\n"
                + "            }\n"
                + "        </script>"
                + "        <!--<script src=\"https://code.jquery.com/jquery.js\"></script>-->\n"
                + "        <script src=\"js/jquery-1.11.2.js\"></script>\n"
                + "        <script src=\"js/bootstrap.min.js\"></script>\n"
                + "    </body>\n"
                + "\n"
                + "</html>");
        db.disconnect();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
