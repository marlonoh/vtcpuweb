/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vtcup.molaya.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.molaya.utils.Utilities;
import org.vtcup.molaya.app.ControllerActiveDirectory;
import org.vtcup.molaya.db.DataBaseConnection;

/**
 *
 * @author molaya
 */
public class ResetPass extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (username != null) {
            request.getSession().setAttribute("user", username);
            response.sendRedirect("index.html");
        } else {
            request.setAttribute("error", "Unknown user, please try again");
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        DataBaseConnection db = new DataBaseConnection();
        ControllerActiveDirectory ad = new ControllerActiveDirectory();
        Utilities genPass = new Utilities();
        HttpSession session = request.getSession();
        ResultSet rs = null;
        String user = request.getParameter("username");
        String pass = request.getParameter("newpassword");
        String userid = null;
        String userinfo = null;

        String userdsn = null;
        String msg = null;

        db.connect();
        boolean isReseted = false;
        boolean blocked = false;

        //Validating session.
        userid = (String) session.getAttribute("userid");
        System.out.println("userid: " + userid);
        userinfo = (String) session.getAttribute("userinfo");
        System.out.println("userinfo: " + userinfo);

        if (user.contains("administrator") || user.contains("admin") || user.contains("chgpass")) {
            blocked = true;
            db.write("UPDATE TBLUSERS SET status=\"DISABLED\" WHERE  userid=\"" + userid + "\"");
            response.sendRedirect("index.html?error=405");
            return;
        }
        
        if (userid == null || userinfo == null) {
            session.invalidate();
            response.sendRedirect("index.html?error=440");
        }
        userdsn = ad.getObjectDSN(user, "user");

        System.out.println("THE NEW PASSWORD IS:" + pass);

        if (pass == null || pass.isEmpty()) {
            pass = genPass.generatePassword();
            System.out.println("THE GENERATED PASSWORD IS:" + pass);
        }

        if (!blocked) {
            isReseted = ad.resetPasswordUser(userdsn, pass);
        }
        
        System.out.println("RESET SESSIONID:" + session.getId());
        out.println("<!DOCTYPE html>\n"
                + "<!--\n"
                + "We are working for improving your Active Directory experience.\n"
                + "You must validate all info with the application manager.\n"
                + "If you ahve any sugestion, please send email to olaya.marlon@gmail.com.\n"
                + "-->\n"
                + "<html lang=\"en\">\n"
                + "    <head>\n"
                + "        <meta charset=\"utf-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "        <title>Vertical Active Directory Tools</title>\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "        <link href=\"css/animate.css\" rel=\"stylesheet\">\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <h2 style=\"text-align: center;\">Reset Password Status</h2>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"modal-dialog\">\n"
                + "                <div class=\"modal-content\">\n"
                + "                    <div class=\"modal-header\">\n"
                + "                        <!--<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>-->\n"
                + "                        <div class=\"row text-center animated fadeInDown\"><img src=\"images/logocrediorbe.png\"></div>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-body \">\n"
                + "                            <dl class=\"dl-horizontal\">\n"
                + "                        <h3>\n"
                + "                                <dt>Username:</dt> <dd>" + user + "</dd>\n"
                + "                                <dt>Password:</dt> <dd>" + pass + "</dd>\n"
                + "                                <dt>Group:</dt> <dd>" + ad.getGroupOfObjectDSN(userdsn, null, true) + "</dd>\n");
        if (isReseted) {
            out.println("                                <dt>Status:</dt> <dd><span class=\"label label-success animated tada\">Success</span></dd>\n");
            db.write("INSERT INTO TBLLOGS (userid, logname, loginfo) VALUES (" + userid + ", 'RESETPASSWORD', 'RESETING PASSWORD TO USER: " + user + " STATUS: SUCCESS' )");
        } else {
            if (userdsn != null) {
                msg = "We can't reset the password of " + userdsn + "\n" + ad.getErrors();
            } else {
                msg = "Wrong username: " + ad.getErrors();
            }
            out.println("                                <dt>Status:</dt> <dd><span class=\"label label-danger\">Unsuccessfull</span></dd>\n"
                    + "                        </h3> \n"
                    + "                                <dt>Reason:</dt> <dd>" + msg + "</dd>");
            // Saving data on Database
            db.write("INSERT INTO TBLLOGS (userid, logname, loginfo) VALUES (" + userid + ", 'RESETPASSWORD', 'RESETING PASSWORD TO USER: " + user + " STATUS: UNSUCCESSFULL' )");
        }
        out.println("                            </dl>  \n"
                + "                    </div>\n"
                + "                    <div class=\"modal-footer\">\n"
                + "                        <button class=\"btn btn-lg btn-default\" onclick=\"myFunction()\" ><span class=\"glyphicon glyphicon-print\"></span> Print</button>\n");
        out.println("                        <button class=\"btn btn-lg btn-primary animated swing\" onclick=\"window.location='home.html'\"><span class=\"glyphicon glyphicon-home\"></span> Go back!</button>");
        out.println("                    </div>\n"
                + "\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>\n"
                + "        <footer class=\"footer\">\n"
                + "            <div class=\"container\">\n"
                + "                <div class=\"row text-center animated fadeInUp\"><img src=\"images/logo.png\"></div>\n"
                + "            </div>\n"
                + "        </footer>\n"
                + "        <script>"
                + "            function myFunction() {\n"
                + "                window.print();\n"
                + "            }\n"
                + "        </script>"
                + "        <!--<script src=\"https://code.jquery.com/jquery.js\"></script>-->\n"
                + "        <script src=\"js/jquery-1.11.2.js\"></script>\n"
                + "        <script src=\"js/bootstrap.min.js\"></script>\n"
                + "    </body>\n"
                + "\n"
                + "</html>");
        db.disconnect();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
