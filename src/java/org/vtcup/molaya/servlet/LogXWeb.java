/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vtcup.molaya.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.vtcup.molaya.db.DataBaseConnection;

/**
 *
 * @author molaya
 */
@WebServlet(name = "logXWeb", urlPatterns = {"/logXWeb"})
public class LogXWeb extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");

        //Declaración de Variables
        HttpSession session = null;
        PrintWriter out = null;
        Cookie cook = null;
        DataBaseConnection db = new DataBaseConnection();
        String user = request.getParameter("username");
        String pass = request.getParameter("password");
        String action = request.getParameter("action");
        String userid = null;
        String msg = null;
        ResultSet rs = null;
        boolean status = false;
        boolean statusAccount = false;
        boolean wrongUser = false;
        System.out.println("SEARCHING " + user);
        db.connect();
        session = request.getSession(true);
        out = response.getWriter();
        try {

            if (action.equals("Logout")) {
                rs = db.read("SELECT userid from 'TBLSESSIONS' where sessionid=\"" + session.getId() + "\"");
                userid = rs.getString("userid");
                rs = db.read("SELECT * from 'TBLUSERS' where userid=\"" + userid + "\"");
                System.out.println("LOGOUT...");
                session.invalidate();
                Cookie cooks[] = request.getCookies();
                String cmd = "INSERT INTO TBLLOGS(logname, userid, loginfo) VALUES(\"LOGOUT\"," + userid + ",\"LOGOUT " + rs.getString("userinfo") + "\")";
                if (cooks != null) {
                    for (int i = 0; i < cooks.length; i++) {
                        System.out.println(cooks[i].getValue());
                        cooks[i].setMaxAge(0);
                        response.addCookie(cooks[i]);
                        System.out.println(cooks[i].getValue());
                    }
                }
                status = db.write(cmd);
                System.out.println(cmd + "Logout Status= " + status);
            } else {
                rs = db.read("SELECT * from 'TBLUSERS' where username=\"" + user + "\"");
                userid = rs.getString("userid");
                
                if (rs.getString("status").equalsIgnoreCase("enable")) {
                        statusAccount = true;
                }

                System.out.println("VALIDATING ****, " + rs.getString("username") + " ID:" + rs.getString("userid"));
            }
            if (!user.isEmpty() && !rs.getString("username").isEmpty() && user.equals(rs.getString("username")) && pass.matches(rs.getString("password")) && statusAccount) {
                msg = rs.getString("userinfo");
                System.out.println("WELCOME, " + msg + "!");
                status = db.write("INSERT INTO TBLLOGS(logname, userid, loginfo) VALUES(\"LOGIN\"," + userid + ", \"LOGIN " + msg + "\")");
                System.out.println("SESSIONID: " + session.getId() + "!");
                status = db.write("INSERT INTO TBLSESSIONS(sessionid, userid) VALUES(\"" + session.getId() + "\"," + userid + ")");
                cook = new Cookie("sessionInfo", session.getId());
                cook.setMaxAge(1800);
                session.setAttribute("userinfo", msg);
                session.setAttribute("userid", userid);
                response.addCookie(cook);
                cook = new Cookie("userName", msg);
                cook.setMaxAge(1800);
                response.addCookie(cook);
                response.sendRedirect("home.html");
            } else {
                if (action.equals("Logout")) {
                    response.sendRedirect("index.html");
                } else if (!statusAccount) {
                    response.sendRedirect("index.html?error=405");
                } else {
                    response.sendRedirect("index.html?error=401");
                }
            }
            if (!status) {
                System.out.println("FAIL, Insert log session status");
            }
            db.disconnect();
        } catch (SQLException e) {
            System.out.println("Exception :" + e);
            response.sendRedirect("index.html?error=401" + e);
            db.disconnect();
        } catch(RuntimeException e){
            response.sendRedirect("index.html?error=500" + e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
