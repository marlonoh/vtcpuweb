/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vtcup.molaya.app;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.molaya.utils.*;

/**
 *
 * @author molaya
 */
public class ControllerActiveDirectory {
// Execute command

    RuntimeExec runTimex;
    private String objectDsn;
    private String error;
    private String USERAD;
    private String PASSAD;
    private String IPAD;
    private String UPNAD;

    public String getIpAD() {
        return IPAD;
    }

    public void setIpAD(String IPAD) {
        this.IPAD = IPAD;
    }

    public String getUPNAD() {
        return UPNAD;
    }

    public void setUPNAD(String UPN) {
        this.UPNAD = IPAD;
    }

    /**
     * Constructor
     */
    public ControllerActiveDirectory() {
        this.runTimex = new RuntimeExec();
        this.error = "";
        USERAD = "chgpass";
        PASSAD = "Gesti0nCOrb3";
        IPAD = "10.201.1.15";
        UPNAD = "crediorbe.local";
    }

    /**
     * This validate if the user exist in the AD
     *
     * @param user
     * @return
     */
    private boolean validateUser(String user) {
        return this.getObjectFromAD(user, "user");
    }

    /**
     * Set new password Active Directory
     *
     * @param pwdad
     */
    public void setPassUsrAd(String pwdad) {
        PASSAD = pwdad;
    }

    /**
     * Get String ObjectDSN
     *
     * @return
     */
    public String getObjectString() {
        return objectDsn;
    }

    /**
     * Get String Errors
     *
     * @return
     */
    public String getErrors() {
        return error;
    }

    /**
     * Set User with Administrator permissions to Send commands
     *
     * @param usrad
     */
    public void setAdminUsrAd(String usrad) {
        USERAD = usrad;
    }

    /**
     * Get DSN of users, computers, groups from AD.
     *
     * @param name Name, alias or samid of object
     * @param type [user/computer/group]
     * @return [true/false]
     */
    public boolean getObjectFromAD(String name, String type) {
        String command = "dsquery " + type + " -samid " + name + " -s " + IPAD + " -u " + USERAD + " -p " + PASSAD;
        if (type.equalsIgnoreCase("computer")) {
            command = command.replace("-samid", "-name");
        }
        System.out.println("Active Controller: " + command);
        runTimex.runProcess(command);
        objectDsn = runTimex.getMessage().replace("¡", "í").replace("¢", "ó").replace("‚", "é").replace("£", "ú").replace("ÿþ", "").replace("¤", "ñ");
        if (objectDsn.contains("CN=")) {
            System.out.println(objectDsn);
            return true;
        }
        error += "\n" + runTimex.getMessageError();
        System.out.println(error);
        return false;
    }

    /**
     * Get Group of ObjectDSN
     *
     * @param name
     * @param type
     * @param isDsn
     * @return
     */
    public String getGroupOfObjectDSN(String name, String type, boolean isDsn) {

        if (type == null) {
            type = "user";
        }
        if (!isDsn) {
            name = this.getObjectDSN(name, type);
            if (name == null) {
                return null;
            }
        }
        String group = null;
        String command = "dsget user " + name + " -memberof ";
        command += "-u " + USERAD + " -p " + PASSAD + " -s " + IPAD;

        System.out.println("Active Controller: " + command);
        runTimex.runProcess(command);
        if (runTimex.getMessage().toLowerCase().contains("crediall")) {
            System.out.println("OKall---" + runTimex.getMessage());
            group = "CrediAll";
        } else if (runTimex.getMessage().toLowerCase().contains("crediother")) {
            System.out.println("OKother---" + runTimex.getMessage());
            group = "CrediOtherPages";
        } else if (runTimex.getMessage().toLowerCase().contains("credivip")) {
            System.out.println("OKvip---" + runTimex.getMessage());
            group = "CrediVIP";
        } else if (runTimex.getMessage().toLowerCase().contains("usb")) {
            System.out.println("OKvip---" + runTimex.getMessage());
            group = "Block_USB Policy";
        } else {
            System.out.println("WRONG POLICIES---" + runTimex.getMessage());
            error += "\n" + runTimex.getMessage();
            System.out.println("ERROR---" + error);
        }
        return group;
    }

    /**
     * Remove Group and Add to New Group
     *
     * @param name
     * @param type
     * @param isDsn
     * @return
     */
    public boolean changeObjectGroup(String newGroup, String name, String type, boolean isDsn) {

        String command = null;
        String oldGroup = null;

        if (!isDsn) {
            name = this.getObjectDSN(name, type);
            if (name == null) {
                return false;
            }
        }

        oldGroup = this.getGroupOfObjectDSN(name, null, true);
        if (oldGroup == null) {
            if (this.addMembersToGroup(newGroup, name, null, true)) {
                System.out.println("ADDEDED NEW GROUP: " + newGroup + " to user: " + name);
                return true;
            }
        }
        if (this.deleteMembersFromGroup(oldGroup, name, null, true)) {
            System.out.println("DELETED GROUP: " + oldGroup + " to user: " + name);
            if (this.addMembersToGroup(newGroup, name, null, true)) {
                System.out.println("ADDEDED NEW GROUP: " + newGroup + " to user: " + name);
                return true;
            }
        }
        return false;
    }

    /**
     * Add Members to a Specific Group
     *
     * @param group Name of Group
     * @param name Name of Member; could be a DSN name
     * @param type Type [user/computer]
     * @param isDsn [true/false]
     * @return [true/false] Added or not
     */
    public boolean addMembersToGroup(String group, String name, String type, boolean isDsn) {
        if (!isDsn) {
            name = this.getObjectDSN(name, type);
            if (name == null) {
                return false;
            }
        }
        String myGroup = this.getObjectDSN(group, "group");
        String command = "dsmod group "+myGroup+" -addmbr " + name.replace("\n", "");
        command += " -s " + IPAD + " -u " + USERAD + " -p " + PASSAD;
        System.out.println("Active Controller: " + command);
        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OKADDGROUP--" + runTimex.getMessage());
            return true;
        }
        error += "\n" + runTimex.getMessageError();
        System.out.println(error);
        return false;
    }

    public boolean deleteMembersFromGroup(String group, String name, String type, boolean isDsn) {
        if (!isDsn) {
            name = this.getObjectDSN(name, type);
            if (name == null) {
                return false;
            }
        }
        String myGroup = this.getObjectDSN(group, "group");
        String command = "dsmod group "+myGroup+" -rmmbr " + name.replace("\n", "");
        command += " -s " + IPAD + " -u " + USERAD + " -p " + PASSAD;
        System.out.println("Active Controller: " + command);
        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OKADDGROUP--" + runTimex.getMessage());
            return true;
        }
        error += "\n" + runTimex.getMessageError();
        System.out.println(error);
        return false;
    }

    /**
     * Return the DSN Name
     *
     * @param user
     * @return
     */
    public String getUserDSN(String user) {
        if (this.getObjectFromAD(user, "user")) {
            System.out.println("Ok Searching...");
            return objectDsn;
        }
        return null;
    }

    /**
     * Return the DSN Name
     *
     * @param user
     * @return
     */
    public String getObjectDSN(String name, String type) {
        if (this.getObjectFromAD(name, type)) {
            System.out.println("Ok Searching DSN...");
            return objectDsn;
        }
        return null;
    }

    /**
     * Reset password of Active Directory Users by Userdsn
     *
     * @param user Must be the DSN name
     * @param pass
     * @return [TRUE,FALSE]
     */
    public boolean resetPasswordUser(String user, String pass) {
        if (user == null) {
            return false;
        }
        String command = "dsmod user " + user + " -pwd " + pass;
        command += " -mustchpwd yes -u " + USERAD + " -p " + PASSAD + " -s " + IPAD;

        System.out.println("Active Controller: " + command);
        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OK---" + runTimex.getMessage());
            return true;
        }
        if (runTimex.getMessage().contains("Check the")) {
            System.out.println("WRONG POLICIES---" + runTimex.getMessage());
        }
        error += "\n" + runTimex.getMessage();
        System.out.println("ERROR---" + error);
        return false;
    }

    /**
     * Reset password of Active Directory Users by username
     *
     * @param user Must be te DSN name
     * @param pass
     * @return [TRUE,FALSE]
     */
    public boolean resetPasswordUsername(String user, String pass) {
        if (user == null) {
            return false;
        }
        String command = "dsquery user -samid " + user + " -u " + USERAD + " -p " + PASSAD + " -s " + IPAD + " | dsmod -pwd " + pass;
        command += " -mustchpwd yes -u " + USERAD + " -p " + PASSAD + " -s " + IPAD;

        System.out.println("Active Controller: " + command);

        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OK---" + runTimex.getMessage());
            return true;
        }
        if (runTimex.getMessage().contains("Check the")) {
            System.out.println("WRONG POLICIES---" + runTimex.getMessage());
        }
        error += "\n" + runTimex.getMessage();
        System.out.println("ERROR---" + error);
        return false;
    }

    /**
     * Reset password of Active Directory Users by username
     *
     * @param user Must be te DSN name
     * @param pass
     * @return
     */
    public boolean addUserToAD(String user, String pass, String samid, String office, String group) {
        if (user == null) {
            return false;
        }
//        String command = "dsquery group -samid " + group;
//        command += " -u " + USERAD + " -p " + PASSAD + " -s " + IPAD;
//        runTimex.runProcess(command);
//        System.out.println("Active Controller: " + command + "GROUP:" + runTimex.getMessage());

        String command = "dsadd user \"CN=" + user + ",OU=Usuarios,OU=" + office + ",DC=crediorbe,DC=local\" -samid " + samid + " -pwd " + pass;
        command += " -upn " + samid + "@" + UPNAD; //+" -profile \\\\DC-CREDIORBE-02\\profiles\\%username% -hmdir \\\\DC-CREDIORBE-02\\users\\%username%";
        command += " -memberof " + getObjectDSN(group, "group");
        command += " -mustchpwd yes -u " + USERAD + " -p " + PASSAD + " -disabled no -s " + IPAD;

        System.out.println("Active Controller: " + command);

        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OK---" + runTimex.getMessage());
            return true;
        }
        if (runTimex.getMessage().contains("Check the")) {
            System.out.println("WRONG INFORMATION---" + runTimex.getMessage());
        }
        error += "\n" + runTimex.getMessage();
        System.out.println("ERROR---" + error);
       
        return false;
    }

    
    public boolean addRoamingUserToAD(String user, String pass, String samid, String office, String group, String profile, String hmdir) {
        if (user == null) {
            return false;
        }
        String command = "dsadd user \"CN=" + user + ",OU=Usuarios,OU=" + office + ",DC=crediorbe,DC=local\" -samid " + samid + " -pwd " + pass;
        command += " -upn " + samid + "@" + UPNAD+ " -profile " + profile + " -hmdir " + hmdir;//\\\\\\\\DC-CREDIORBE-02\\\\profiles\\\\%username% -hmdir \\\\DC-CREDIORBE-02\\users\\%username%"
        command += " -memberof " + getObjectDSN(group, "group");
        command += " -mustchpwd yes -u " + USERAD + " -p " + PASSAD + " -disabled no -s " + IPAD;

        System.out.println("Active Controller: " + command);

        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OK---" + runTimex.getMessage());
            return true;
        }
        if (runTimex.getMessage().contains("Check the")) {
            System.out.println("WRONG INFORMATION---" + runTimex.getMessage());
        }
        error += "\n" + runTimex.getMessage();
        System.out.println("ERROR---" + error);
       
        return false;
    }

    /**
     * Remove user or computer from AD
     *
     * @param name
     * @param type
     * @param isDsn
     * @return
     */
    public boolean remObjectFromAD(String name, String type, boolean isDsn) {

        String command = null;
        if (!isDsn) {
            name = this.getObjectDSN(name, type);
            if (name == null) {
                return false;
            }
        }
        command = "dsrm " + name;
        command += " -noprompt -u " + USERAD + " -p " + PASSAD + " -s " + IPAD;

        System.out.println("Active Controller: " + command);

        runTimex.runProcess(command);
        if (runTimex.getMessage().contains("succeeded:CN=")) {
            System.out.println("OK---" + runTimex.getMessage());
            return true;
        }
        if (runTimex.getMessage().contains("Check the")) {
            System.out.println("WRONG POLICIES---" + runTimex.getMessage());
        }
        error += "\n" + runTimex.getMessage();
        System.out.println("ERROR---" + error);
        return false;
    }
}
