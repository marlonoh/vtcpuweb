package org.vtcup.molaya.db;

import java.sql.*;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseConnection {

    private ResultSet row;
    private Connection con = null;
    Statement stmt = null;

    private String driver;
    private final String servername = "localhost";
    private final String port = "8080";
    private final String dataBase;
    private final String user = "";
    private final String pass = "";
    private final String url;

    public DataBaseConnection() {
        this.dataBase = "VTDatabase.db";
        this.driver = "org.sqlite.JDBC";
        this.url = "jdbc:sqlite:" + this.dataBase;
    }

    public DataBaseConnection(String dbname) {
        this.dataBase = dbname;
        this.driver = "org.sqlite.JDBC";
        this.url = "jdbc:sqlite:" + this.dataBase;
    }

    public Connection connect() {
        ResultSet rs = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url);
            stmt = con.createStatement();
            System.out.print("CONEXION CORRECTA A DATABSE: " + this.dataBase);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.print("ERROR EN LA CONEXION: " + e);
        }
        return con;
    }

    public void disconnect() {
        try {
            stmt.close();
            con.close();
            System.out.print("DISCONNECTION DONE!");
        } catch (Exception e) {
            System.out.print("DISCONNECTION ERROR: " + e);
        }
    }

    public Statement getStatement() {
        return stmt;
    }

    /**
     * Write info in database method.
     *
     * @param query
     * @return true if write ok, false if not
     */
    public boolean write(String query) {
        try {
            stmt.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println("DATABASE WRITTING ERROR: " + ex);
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Read info from database method.
     *
     * @param query
     * @return true if write ok, false if not
     */
    public ResultSet read(String query) {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(query);
            return rs;
        } catch (Exception e) {
            System.out.print("READ ERROR, the reason is: " + e);
            return null;
        }
    }

}
